import json
from shapely.geometry import shape, Point, LineString
from math import cos, sin, asin, sqrt, radians, pi
import numpy as np
from time import time
from pyproj import Proj


def square_dist(Point1, Point2):
    x1, y1 = Point1.coords[0]
    x2, y2 = Point2.coords[0]
    square_dist = (x1 - x2) ** 2 + (y1 - y2) ** 2
    return square_dist


def is_near(Point1, Point2, distance):
    return square_dist(Point1, Point2) <= distance**2


class FiberGeometry():

    def calc_nearby_channels(self, max_distance):
        self.near_channels = {}
        for i_s1, s1 in enumerate(self.segments_xy):
            print i_s1
            for i_p1, p1 in enumerate(s1.points):
                p1_circle = p1.buffer(max_distance)
                #print i_p1
                p1_near = []
                for i_s2, s2 in enumerate(self.segments_xy):
                    intersection = p1_circle.intersection(s2)
                    if not intersection.is_empty:  # then it is a linestring of 2 points
                        p1_near += self.segment_part_to_channels(s2, intersection)
                self.near_channels[p1.channel] = p1_near

    def channel_to_Point_xy(self, channel):
        for i in xrange(len(self.segments_xy)):
            if channel < self.segments_xy[i].last_channel:
                seg = i
                break
        assert seg is not None
        dist_norm = float(channel - self.segments_xy[seg].start_channel)/self.segments_xy[seg].channel_count
        point = self.segments_xy[seg].interpolate(dist_norm, normalized=True)
        point.channel = channel
        point.dist_norm = dist_norm
        return point

    def channel_to_Point_latlon(self, channel):
        for i in xrange(len(self.segments_latlon)):
            if channel < self.segments_latlon[i].last_channel:
                seg = i
                break
        assert seg is not None
        dist_norm = float(channel - self.segments_latlon[seg].start_channel)/self.segments_latlon[seg].channel_count
        point = self.segments_latlon[seg].interpolate(dist_norm, normalized=True)
        point.channel = channel
        point.dist_norm = dist_norm
        return point

    def segment_part_to_channels(self, segment, part):
        l1 = segment.project(Point(part.coords[0]), normalized=True)
        l2 = segment.project(Point(part.coords[1]), normalized=True)
        m1 = min(l1, l2)
        m2 = max(l1, l2)
        start_ch = int(segment.start_channel + segment.channel_count*m1)
        end_ch = int(segment.start_channel + segment.channel_count*m2)
        return range(start_ch, end_ch)

    def GetChannelCoordinates(self, channel):
        for i in xrange(len(self.segments_latlon)):
            if channel < self.segments_latlon[i].last_channel:
                seg = i
                break
        dist_norm = float(channel - self.segments_latlon[seg].start_channel)/self.segments_latlon[seg].channel_count
        point = self.segments_latlon[seg].interpolate(dist_norm, normalized=True)
        return point.coords[0]

    def __init__(self, file_name):
        with file(file_name, 'r') as f:
            js = json.load(f)
            feature = js['features'][0]
            lon, lat = zip(*shape(feature['geometry']).coords)
            project = Proj("+proj=aea +lat_1={} +lat_2={} +lat_0={} +lon_0={}"
                .format(min(lat), max(lat), np.average(lat), np.average(lon)))
            x, y = project(lon, lat)
            coords_xy = zip(x, y)
            coords_latlon = zip(lon, lat)
            cop = {"type": "LineString", "coordinates": coords_xy}
            self.segments_xy = map(LineString, zip(coords_xy[:-1], coords_xy[1:]))
            self.segments_latlon = map(LineString, zip(coords_latlon[:-1], coords_latlon[1:]))
            last_channels = np.cumsum(feature['geometry']['counts'])
            start_channels = np.append([0], last_channels[:-1])
            for s in xrange(len(self.segments_xy)):
                self.segments_xy[s].start_channel = start_channels[s]
                self.segments_xy[s].last_channel = last_channels[s]
                self.segments_xy[s].channel_count = feature['geometry']['counts'][s]
                self.segments_xy[s].points = \
                    map(self.channel_to_Point_xy, xrange(self.segments_xy[s].start_channel, self.segments_xy[s].last_channel))

                self.segments_latlon[s].start_channel = start_channels[s]
                self.segments_latlon[s].last_channel = last_channels[s]
                self.segments_latlon[s].channel_count = feature['geometry']['counts'][s]
                self.segments_latlon[s].points = \
                    map(self.channel_to_Point_latlon, xrange(self.segments_latlon[s].start_channel, self.segments_latlon[s].last_channel))

            print "Total length (m):", int(shape(cop).length)

            self.channel_count = self.segments_xy[-1].last_channel
            print "Total channels: ", self.channel_count

            # self.calc_nearby_channels(50)
            # print self.near_channels[0]
            # print self.near_channels[1]
            # print self.near_channels[2]


def main():
    start_time = time()
    f = FiberGeometry('path1.json')
    print "Load time (s):", time()-start_time

    print f.GetChannelCoordinates(0)
    print f.GetChannelCoordinates(7029)

if __name__ == '__main__':
    main()
